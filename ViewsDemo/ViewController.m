//
//  ViewController.m
//  ViewsDemo
//
//  Created by James Cash on 17-04-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import "ViewController.h"
#import "SecondViewController.h"

@interface ViewController ()
//@property (strong, nonatomic) IBOutlet UIView *greenView;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (nonatomic,strong) UIView* someNewView;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];

//    self.greenView.layer.cornerRadius = 20.0;

    CGPoint myPoint = CGPointMake(10, 50);
    myPoint.x;
    myPoint.y;
    NSLog(@"Point = %@", NSStringFromCGPoint(myPoint));

    CGRect myRect = CGRectMake(0, 10, 50, 70);
    myRect.origin.x;
    CGPoint myRectOrigin = myRect.origin;
    myRect.size.width;
    NSInteger centerX = CGRectGetMidX(myRect);

    self.someNewView = [[UIView alloc] initWithFrame:myRect];
    self.someNewView.backgroundColor = [UIColor redColor];
    [self.view addSubview:self.someNewView];

    UIView *otherView = [[UIView alloc] initWithFrame:CGRectMake(10, 10, 20, 20)];
    otherView.backgroundColor = [UIColor blueColor];
    [self.someNewView addSubview:otherView];
}

//- (void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {

- (IBAction)growOurWeirdView:(id)sender {
    [UIView animateWithDuration:2 animations:^{
        CGRect oldFrame = self.someNewView.frame;
        CGRect newFrame = CGRectOffset(oldFrame, 50, 150);
        CGRect newNewFrame = CGRectInset(newFrame, -50, -50);
        self.someNewView.frame = newNewFrame;
    } completion:^(BOOL finished) {
        NSLog(@"Animation finished %d", finished);
    }];

    // another example use of a block
    [@[@1, @2, @3] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL* stop) {
        NSLog(@"Item #%ld is %@", idx, obj);
    }];
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    NSLog(@"Should perform segue %@? (from %@)", identifier, sender);
    if ([identifier isEqualToString:@"showYellowSegue"]) {
        NSLog(@"Not showing yellow");
        return YES;
    }
    return YES;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"showYellowSegue"])
    {
        SecondViewController *dest = segue.destinationViewController;
        NSLog(@"Switching to %@", dest);
        NSString *textToShow = self.textField.text;
        NSLog(@"Setting yellow label to %@", textToShow);
        // this won't work, because at this time the view for the secondviewcontroller hasn't been loaded yet
//        dest.yellowLabel.text = textToShow;
        dest.textToShow = textToShow;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)bloop:(UIButton *)sender {
    NSLog(@"bloop");
}

@end
