//
//  SecondViewController.h
//  ViewsDemo
//
//  Created by James Cash on 18-04-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SecondViewController : UIViewController
// probably this outlet should be in the .m
@property (strong, nonatomic) IBOutlet UILabel *yellowLabel;

@property (strong,nonatomic) NSString *textToShow;

@end
