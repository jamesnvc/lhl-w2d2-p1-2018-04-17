//
//  ViewController.h
//  ViewsDemo
//
//  Created by James Cash on 17-04-18.
//  Copyright © 2018 Occasionally Cogent. All rights reserved.
//

#import <UIKit/UIKit.h>

/** This our custom subclass of UIViewController for a demo
 */
@interface ViewController : UIViewController

@property (nonatomic,strong) IBInspectable UIColor *theColour;

@end

